$(function () {

  //RENDER CARDS FUNCTION
  function renderCards(arr) {
    arr.forEach(function (el) {
      $("#bikes").append(`<div class="col-4 my-2">
		<a href="#" class="text-decoration-none">
			<div class="card">
				<div class="card-img" style="background-image: url('./img/${el.image}.png')"></div>
				<div class="card-body">
					<h6 class="card-title">${el.name}</h6>
					<p class="card-text">${el.price}$</p>
				</div>
			</div>
		</a>
	</div>`);
    });
  }

  //FILTER FUNCTION
  function cardFilter(arr, prop, val) {
    return arr.filter(function (el) {
      return el[prop] == val;
    });
  }

//AJAX REQUEST
  $.get("https://json-project3.herokuapp.com/products")
  .then(function (data) {
    bikes = data;
	renderCards(bikes);
	
	// NUMBER OF BIKES IN SPAN
    $(".show-all").find("span").text(bikes.length);

    $('.filter').each(function(i, el){
		let current = $(this).find("p");
		let txt = current.text().toUpperCase();
		let prop = "";
  
		if ($(this).hasClass("gender")) {
		  prop = "gender";
		} else {
		  prop = "brand";
		}
  
		let n = cardFilter(bikes, prop, txt);
		$(this).find("span").text(n.length);
		})

	//FILTER ON CLICK AND SHOW BIKES
    $(".filter").on("click", function (e) {
      e.preventDefault();

      $("#bikes").html("");

      let clicked = $(this).find("p");
      let text = clicked.text().toUpperCase();
      let type = "";

      if ($(this).hasClass("gender")) {
        type = "gender";
      } else {
        type = "brand";
      }

      let filtered = cardFilter(bikes, type, text);

      renderCards(filtered);
    });

	//SHOW ALL
    $(".show-all").on("click", function (e) {
      e.preventDefault();
      $("#bikes").html("");
      renderCards(bikes);
	});
	
  });




  //   ____end_____
});
